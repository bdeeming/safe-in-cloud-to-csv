# README #
## How do I use the script? ##

1. Download & install python 2.x
1. Download the [script](https://bitbucket.org/bdeeming/safe-in-cloud-to-csv/downloads/safeincloudxmltocsv-1_0.py) to your home area
1. Open SafeInCloud, choose File > Export > As XML
1. Save the file somewhere
1. Open up a cmd prompt (Start > Type cmd > press enter)
1. Run the script with `C:\Python27\python.exe safeincloudxmltocsv-1_0.py <path-to-exported-xml-file> convertedFile.csv` 
1. The resulting file will most likely be in your home area (where the cmd prompt opens by default)
1. Follow the instructions printed out by the script to import into KeePass (or whatever other program you're going to be using)