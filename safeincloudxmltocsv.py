#!/usr/bin/env python
'''
Created on 6/01/2015

@author: bdeeming
'''

import sys
import xml.etree.ElementTree as ET
import csv

if __name__ == '__main__':
    inputFileName = sys.argv[1]
    outputFileName = sys.argv[2] 
    
    root = ET.parse(inputFileName).getroot()
    
    for card in root.findall("./card[@deleted='true']"):
        root.remove(card)
        print 'Removed:', card.attrib['title']
    
    # Find all the groups
    groups = {label.attrib['id']: label.attrib['name'] for label in root.findall("./label")}
    
    records = []
    for card in root.findall("./card"):
        print 'Importing card:', card.attrib['title'],
        
        login = card.find("./field[@type='login']")
        password = card.find("./field[@type='password']")
        url = card.find("./field[@type='website']")
        notes = card.find("./notes")
        groupId = card.find("./label_id")
        
        newRecord = {'Title': card.attrib['title'],
                     'User Name': login.text if login is not None else "", 
                     'Password': password.text if password is not None else "",
                     'URL': url.text if url is not None else "",
                     'Notes': notes.text if notes is not None else "",
                     'Group': groups[groupId.text] if groupId is not None else ""}
        
        # Remove to prevent duplicating the data in the string fields
        if login is not None:
            card.remove(login)
        if password is not None:
            card.remove(password)
        if url is not None:
            card.remove(url)
        
        # All remaining fields are string fields ('S:' to avoid conflicts with fields above)
        newRecord.update({'S:' + field.attrib['name']: field.text for field in card.findall("./field")})
        
        # Special group for template cards
        if card.get('template') == 'true':
            newRecord['Group'] = 'Templates'
            # Put something in the field so it doesn't get deleted by the XML Replace Tool
            newRecord.update({'S:' + field.attrib['name']: '-' for field in card.findall("./field")})
            print '(template)',
        print
        
        records.append(newRecord)
        
    # Export to CSV
    print 'Writing to file:', outputFileName
    with open(outputFileName, 'wb') as csvfile:
        fieldNames = ['Title', 'User Name', 'Password', 'URL', 'Notes', 'Group']
        additionalFields = {key for x in records for key in x.keys()} - set(fieldNames)
        fieldNames.extend(additionalFields)
        
        writer = csv.DictWriter(csvfile, fieldnames=fieldNames)
        writer.writeheader()
        writer.writerows(records)
        print 'Complete'
        
        print '--------------------------------------------------'
        print 'Import into KeePass using the Generic CSV Importer'
        print 'Use the following fields in this order:'
        for field in fieldNames:
            print '\t' + field.replace('S:', '')
            
        print '\nAfter import, remove any empty fields by running Tools > Database Tools > XML Replace...'
        print "Enter the path: //Entry/String[Value='']"
        print 'Select: Remove Nodes'
        print 'Click OK'
        print "Repeat with path: //Entry/String[Value='-' and ./../../Name='Templates']/Value"
        print "Select: Replace"
        print "Select: Inner Text"
        print "Find What: -"
        print "Leave 'Replace With' empty"
        print 'Click OK'
        print 'Save your database'
        
        
